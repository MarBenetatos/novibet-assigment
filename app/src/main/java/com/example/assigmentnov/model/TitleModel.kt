package com.example.assigmentnov.model

import kotlinx.parcelize.Parcelize

@Parcelize
data class TitleModel(
    val title: Int,
    override val type: Int
) : CustomViewTypes(type)