package com.example.assigmentnov.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
open class CustomViewTypes(open val type: Int) : Parcelable{
    companion object{
        const val TitleView : Int =0
        const val GameView : Int =1
    }
}
