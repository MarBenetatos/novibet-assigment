package com.example.assigmentnov.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UiModel(
    var games: MutableList<CustomViewTypes> = mutableListOf(),
    var headlines: MutableList<HeadlinesModel> = mutableListOf(),
    var token: String = ""
) : Parcelable

