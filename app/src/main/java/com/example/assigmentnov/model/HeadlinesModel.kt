package com.example.assigmentnov.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HeadlinesModel(
    val competitor1Caption: String,
    val competitor2Caption: String,
    val startTime: String
) : Parcelable