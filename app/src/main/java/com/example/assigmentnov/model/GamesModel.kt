package com.example.assigmentnov.model

import kotlinx.parcelize.Parcelize

@Parcelize
data class GamesModel(
    val competitor1: String,
    val competitor2: String,
    val elapsed: String,
    override val type: Int
) : CustomViewTypes(type)