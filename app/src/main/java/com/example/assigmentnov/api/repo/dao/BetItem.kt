package com.example.assigmentnov.api.repo.dao

data class BetItem(
    val caption: String,
    val code: String,
    val id: Int,
    val instanceCaption: Any,
    val isAvailable: Boolean,
    val oddsText: String,
    val price: Double
)