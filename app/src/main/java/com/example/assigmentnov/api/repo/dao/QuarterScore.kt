package com.example.assigmentnov.api.repo.dao

data class QuarterScore(
    val awayScore: Int,
    val caption: String,
    val homeScore: Int
)