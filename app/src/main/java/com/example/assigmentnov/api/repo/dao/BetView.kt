package com.example.assigmentnov.api.repo.dao

data class BetView(
    val betViewKey: String,
    val competitionContextCaption: String,
    val competitions: List<Competition>,
    val marketCaptions: List<MarketCaption>,
    val totalCount: Int,
    val betContextId: Int,
    val betItems: List<BetItem>,
    val competitor1Caption: String,
    val competitor2Caption: String,
    val displayFormat: String,
    val imageId: Int,
    val liveData: LiveData,
    val marketTags: List<Any>,
    val marketViewGroupId: Int,
    val marketViewId: Int,
    val modelType: String,
    val path: String,
    val rootMarketViewGroupId: Int,
    val startTime: String,
    val text: String,
    val url: Any,
    val title: String
)