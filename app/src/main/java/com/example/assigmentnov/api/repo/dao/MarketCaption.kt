package com.example.assigmentnov.api.repo.dao

data class MarketCaption(
    val betCaptions: Any,
    val betTypeSysname: String,
    val marketCaption: String
)