package com.example.assigmentnov.api.repo.dao

data class GamesInitDaoItem(
    val betViews: List<BetView>,
    val caption: String,
    val hasHighlights: Boolean,
    val marketViewKey: String,
    val marketViewType: String,
    val modelType: String,
    val totalCount: Int
)