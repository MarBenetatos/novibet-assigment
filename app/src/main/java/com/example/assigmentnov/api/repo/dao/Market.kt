package com.example.assigmentnov.api.repo.dao

data class Market(
    val betItems: List<BetItem>,
    val betTypeSysname: String,
    val marketId: Int
)