package com.example.assigmentnov.api.repo

import com.example.assigmentnov.api.Services
import javax.inject.Inject

class Repository @Inject
constructor(private val service: Services) {
    suspend fun getToken(username: String, password: String) = service.getToken(username, password)
    suspend fun getgames(token: String) = service.getGames(token)
    suspend fun getHeadlines(token: String) = service.getHeadlines(token)
    suspend fun getUpdateHeadlines(token: String) = service.getUpdatedHeadlines(token)
    suspend fun getUpdateGames(token: String) = service.getUpdateGames(token)
}