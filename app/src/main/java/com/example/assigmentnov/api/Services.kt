package com.example.assigmentnov.api

import com.example.assigmentnov.api.repo.dao.GamesInitDao
import com.example.assigmentnov.api.repo.dao.HeadlinesInitDao
import com.example.assigmentnov.api.repo.dao.Token
import retrofit2.Response
import retrofit2.http.*

interface Services {

    @POST("5d8e4bd9310000a2612b5448")
    suspend fun getToken(
        @Query("userName") username : String,
        @Query("userName") password : String,
    ) : Response<Token>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("5d7113513300000b2177973a")
    suspend fun getGames(
        @Header("Authorization") auth: String?
    ): Response<GamesInitDao>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("5d7113ef3300000e00779746")
    suspend fun getHeadlines(
        @Header("Authorization") auth: String?
    ): Response<HeadlinesInitDao>


    //update calls

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("5d711461330000d135779748")
    suspend fun getUpdatedHeadlines(
        @Header("Authorization") auth: String?
    ): Response<HeadlinesInitDao>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("5d7114b2330000112177974d")
    suspend fun getUpdateGames(
        @Header("Authorization") auth: String?
    ): Response<GamesInitDao>
}