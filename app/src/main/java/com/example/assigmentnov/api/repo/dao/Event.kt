package com.example.assigmentnov.api.repo.dao

data class Event(
    val additionalCaptions: AdditionalCaptions,
    val betContextId: Int,
    val hasBetContextInfo: Boolean,
    val isHighlighted: Boolean,
    val liveData: LiveData,
    val markets: List<Market>,
    val path: String
)