package com.example.assigmentnov.api.repo.dao

data class Competition(
    val betContextId: Int,
    val caption: String,
    val events: List<Event>,
    val regionCaption: String
)