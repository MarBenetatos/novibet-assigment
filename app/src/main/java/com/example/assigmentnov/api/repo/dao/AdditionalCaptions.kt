package com.example.assigmentnov.api.repo.dao

data class AdditionalCaptions(
    val competitor1: String,
    val competitor1ImageId: Int,
    val competitor2: String,
    val competitor2ImageId: Int,
    val type: Int
)