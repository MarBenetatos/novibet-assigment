package com.example.assigmentnov.api.repo.dao

data class LiveData(
    val awayCorners: Int,
    val awayGoals: Int,
    val awayPenaltyKicks: Int,
    val awayRedCards: Int,
    val awayYellowCards: Int,
    val homeCorners: Int,
    val homeGoals: Int,
    val homePenaltyKicks: Int,
    val homeRedCards: Int,
    val homeYellowCards: Int,
    val adjustTimeMillis: Int,
    val awayPoints: Int,
    val duration: String,
    val durationSeconds: Int,
    val elapsed: String,
    val elapsedSeconds: Double,
    val homePoints: Int,
    val homePossession: Boolean,
    val isInPlay: Boolean,
    val isInPlayPaused: Boolean,
    val isInterrupted: Boolean,
    val isLive: Boolean,
    val liveStreamingCountries: String,
    val phaseCaption: String,
    val phaseCaptionLong: String,
    val phaseSysname: String,
    val quarterScores: List<QuarterScore>,
    val referenceTime: String,
    val referenceTimeUnix: Int,
    val remaining: String,
    val remainingSeconds: Double,
    val sportradarMatchId: Int,
    val supportsAchievements: Boolean,
    val supportsActions: Boolean,
    val timeToNextPhase: Any,
    val timeToNextPhaseSeconds: Any,
    val timeline: Any
)