package com.example.assigmentnov.api.repo.dao

data class Token(
    val access_token: String,
    val token_type: String
)