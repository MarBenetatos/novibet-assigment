package com.example.assigmentnov.api.repo.dao

data class HeadlinesInitDaoItem(
    val betViews: List<BetView>,
    val caption: String,
    val marketViewKey: String,
    val marketViewType: String,
    val modelType: String
)