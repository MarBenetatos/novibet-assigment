package com.example.assigmentnov.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assigmentnov.databinding.ViewPagerItemBinding
import com.example.assigmentnov.model.HeadlinesModel

class ViewPagerAdapter(var model : MutableList<HeadlinesModel>) : RecyclerView.Adapter<ViewPagerAdapter.ViewHolder> (){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ViewPagerItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.binding(model[position])
    }
    override fun getItemCount(): Int= model.size

    inner class ViewHolder(private val binding :ViewPagerItemBinding) : RecyclerView.ViewHolder(binding.root){

        fun binding(data: HeadlinesModel) {
            binding.competitor1Caption.text = data.competitor1Caption
            binding.competitor2Caption.text = data.competitor2Caption
            binding.startTime.text = data.startTime
        }
    }

    fun updateUi(value: MutableList<HeadlinesModel>) {
        model.clear()
        model.addAll(value)
        notifyDataSetChanged()
    }
}