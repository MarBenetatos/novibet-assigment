package com.example.assigmentnov.adapters

import android.content.Context
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.assigmentnov.R
import com.example.assigmentnov.databinding.RecyclerItemBinding
import com.example.assigmentnov.databinding.RecyclerTitleBinding
import com.example.assigmentnov.model.GamesModel
import com.example.assigmentnov.model.TitleModel
import com.example.assigmentnov.model.CustomViewTypes
import com.example.assigmentnov.utils.TimeUtils
import java.util.concurrent.TimeUnit

class RecyclerAdapter(var model: MutableList<CustomViewTypes>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == CustomViewTypes.GameView) {
            return ViewHolderGames(
                RecyclerItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            return ViewHolderTitle(
                RecyclerTitleBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return model[position].type
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            CustomViewTypes.GameView -> {
                (holder as ViewHolderGames).countDownTimer?.cancel()
                holder.bind(model[position] as GamesModel)
            }
            CustomViewTypes.TitleView -> {

                (holder as ViewHolderTitle).bind(model[position] as TitleModel)
            }
        }
    }

    override fun getItemCount(): Int = model.size

    class ViewHolderTitle(private val binding: RecyclerTitleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(titleModel: TitleModel) {
            binding.textView2.text = binding.root.resources.getString(titleModel.title)
        }
    }

    class ViewHolderGames(private val binding: RecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var countDownTimer: CountDownTimer? = null

        fun bind(gamesModel: GamesModel) {
            binding.elapsed.text = gamesModel.elapsed.substringBefore(".")
            binding.team1.text = gamesModel.competitor1
            binding.team2.text = gamesModel.competitor2
            startTimer(binding.elapsed, gamesModel, binding.root.context)
        }

        private fun startTimer(
            countdownTextureView: TextView,
            data: GamesModel,
            context: Context,
        ): CountDownTimer {

            var timeOfEventMillis: Long =TimeUtils.stringTimeConvertedToMills(data.elapsed)

            return object : CountDownTimer(
                context.resources.getInteger(R.integer.Time_10s).toLong(),
                context.resources.getInteger(R.integer.Time_1s).toLong()
            ) {
                override fun onTick(millisUntilFinished: Long) {
                    timeOfEventMillis = timeOfEventMillis.plus(context.resources.getInteger(R.integer.Time_1s).toLong())
                    countdownTextureView.text = context.getString(
                        R.string.formatted_time,
                        TimeUnit.MILLISECONDS.toHours(timeOfEventMillis) % context.resources.getInteger(R.integer.Time_60),
                        TimeUnit.MILLISECONDS.toMinutes(timeOfEventMillis) % context.resources.getInteger(R.integer.Time_60),
                        TimeUnit.MILLISECONDS.toSeconds(timeOfEventMillis) % context.resources.getInteger(R.integer.Time_60)
                    )
                }
                override fun onFinish() {
                }
            }.start()
        }
    }

    fun updateUi(value: MutableList<CustomViewTypes>) {
        model.clear()
        model.addAll(value)
        notifyDataSetChanged()
    }
}
