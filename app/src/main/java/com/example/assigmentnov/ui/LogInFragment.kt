package com.example.assigmentnov.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.assigmentnov.R
import com.example.assigmentnov.api.ResultOf
import com.example.assigmentnov.databinding.LogInBinding
import com.example.assigmentnov.model.UiModel
import com.example.assigmentnov.viewmodel.AppViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LogInFragment : Fragment(R.layout.log_in) {

    private val viewModel by viewModels<AppViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = LogInBinding.bind(view)
        binding.logIn.setOnClickListener {
            viewModel.logIn(
                binding.username.text.toString(),
                binding.password.text.toString()
            )
            binding.login.visibility=View.GONE
            binding.loader.visibility=View.VISIBLE
        }

        viewModel.obtainResult.observe(viewLifecycleOwner) {
            it?.let {
                when (it) {
                    is ResultOf.Success -> {
                        moveToNextFragment(it.value)
                    }
                    is ResultOf.Failure -> {
                        Toast.makeText(
                            this@LogInFragment.requireActivity(),
                            it.message,
                            Toast.LENGTH_SHORT
                        ).show()

                        binding.login.visibility=View.VISIBLE
                        binding.loader.visibility=View.GONE
                    }
                }
            }
        }
    }

    private fun moveToNextFragment(uiModel: UiModel) {
        val action = LogInFragmentDirections.actionGalleryFragmentToDetailsFragment(uiModel)
        findNavController().navigate(action)
    }
}