package com.example.assigmentnov.ui

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.example.assigmentnov.R
import com.example.assigmentnov.adapters.RecyclerAdapter
import com.example.assigmentnov.adapters.ViewPagerAdapter
import com.example.assigmentnov.api.ResultOf
import com.example.assigmentnov.databinding.DetailsFragmentBinding
import com.example.assigmentnov.viewmodel.AppViewModel
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : Fragment(R.layout.details_fragment) {

    private val args by navArgs<DetailsFragmentArgs>()

    private val viewModel by viewModels<AppViewModel>()
    private lateinit var adapter: ViewPagerAdapter
    private lateinit var recyclerAdapter: RecyclerAdapter


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = DetailsFragmentBinding.bind(view)
        adapter = ViewPagerAdapter(args.model.headlines)
        recyclerAdapter = RecyclerAdapter(args.model.games)

        binding.apply {
            viewPager.adapter = adapter
            TabLayoutMediator(tablayout, viewPager) { _, _ -> }.attach()
            recycler.adapter = recyclerAdapter
        }

        viewModel.obtainResult.observe(viewLifecycleOwner) {
            it?.let {
                when (it) {
                    is ResultOf.Success -> {
                        adapter.updateUi(it.value.headlines)
                        recyclerAdapter.updateUi(it.value.games)
                        viewModel.clearData()
                    }

                    is ResultOf.Failure -> {
                        Toast.makeText(
                            this@DetailsFragment.requireActivity(),
                            it.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
        viewModel.initTimerForUiUpdate(args.model.token)
        startAutoScroll(binding.viewPager)
    }

    private fun startAutoScroll(viewPager: ViewPager2) {
        object : CountDownTimer(5000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                if (viewPager.currentItem == adapter.model.size - 1) viewPager.setCurrentItem(
                    0,
                    true
                ) else
                    viewPager.setCurrentItem(viewPager.currentItem + 1, true)
                startAutoScroll(viewPager)
            }
        }.start()
    }
}