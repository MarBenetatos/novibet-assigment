package com.example.assigmentnov.utils

import java.util.*
import java.util.concurrent.TimeUnit

object TimeUtils {

    fun stringTimeConvertedToMills(elapsed: String):Long = TimeUnit.HOURS.toMillis(elapsed.substring(0, 2).toLong())
        .plus(TimeUnit.MINUTES.toMillis(elapsed.substring(3, 5).toLong()))
        .plus(TimeUnit.SECONDS.toMillis(elapsed.substring(6, 8).toLong()))
}