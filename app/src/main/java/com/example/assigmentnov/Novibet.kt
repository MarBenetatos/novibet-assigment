package com.example.assigmentnov

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Novibet : Application() {
}