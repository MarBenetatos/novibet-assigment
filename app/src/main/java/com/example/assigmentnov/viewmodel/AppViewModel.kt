package com.example.assigmentnov.viewmodel

import android.content.Context
import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.assigmentnov.R
import com.example.assigmentnov.api.ResultOf
import com.example.assigmentnov.api.repo.Repository
import com.example.assigmentnov.api.repo.dao.GamesInitDao
import com.example.assigmentnov.api.repo.dao.HeadlinesInitDao
import com.example.assigmentnov.model.*
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class AppViewModel @Inject constructor(
    private val repo: Repository,
    @ApplicationContext private val context: Context
) : ViewModel() {

    private var uiModel: UiModel = UiModel()
    private val result = MutableLiveData<ResultOf<UiModel>>()
    val obtainResult: LiveData<ResultOf<UiModel>> = result

    fun logIn(username: String, password: String) {
        viewModelScope.launch {
            repo.getToken(username, password).let {
                if (it.isSuccessful) {
                    it.body()?.let { token ->
                        uiModel.token = token.token_type.plus(token.access_token)
                        getInfo(token.token_type.plus(token.access_token))
                        result.postValue(ResultOf.Success(uiModel))
                    }
                } else {
                    result.postValue(ResultOf.Failure(context.getString(R.string.call_error)))
                }
            }
        }
    }

    private suspend fun getInfo(token: String) = withContext(Dispatchers.IO) {
        try {
            listOf(
                launch { uiModel.games.addAll(fetchNeededInfoFromGames(token)) },
                launch { uiModel.headlines.addAll(fetchNeededInfoFromHeaders(token)) }
            ).joinAll()
            false
        } catch (e: Throwable) {
            true
        }
    }

    private suspend fun fetchNeededInfoFromGames(token: String): MutableList<CustomViewTypes> {
        val gamesModel: MutableList<CustomViewTypes> = mutableListOf()
        repo.getgames(token).let {
            if (it.isSuccessful) {
                fillCustomModelGames(gamesModel, it)
            } else {
                result.postValue(ResultOf.Failure(context.getString(R.string.call_error)))
            }
            return gamesModel
        }
    }

    private suspend fun fetchNeededInfoFromHeaders(token: String): List<HeadlinesModel> {
        val response: MutableList<HeadlinesModel> = mutableListOf()
        repo.getHeadlines(token).let {
            if (it.isSuccessful) {
                fillCustomModelHeaders(it, response)
            } else {
                result.postValue(ResultOf.Failure(context.getString(R.string.call_error)))
            }
        }
        return response
    }

    private fun fillCustomModelHeaders(
        data: Response<HeadlinesInitDao>,
        response: MutableList<HeadlinesModel>
    ) {
        data.body()?.get(0)?.let { dao ->
            dao.betViews.filter { it.competitor1Caption != null }.map { betView ->

                response.add(
                    HeadlinesModel(
                        betView.competitor1Caption,
                        betView.competitor2Caption,
                        betView.startTime
                    )
                )
            }
        }
    }

    private fun fillCustomModelGames(
        gamesModel: MutableList<CustomViewTypes>,
        response: Response<GamesInitDao>
    ) {
        gamesModel.add(TitleModel(R.string.football, CustomViewTypes.TitleView))
        response.body()?.let { it ->
            it[0].betViews[0].competitions.map {
                gamesModel.add(
                    GamesModel(
                        it.events[0].additionalCaptions.competitor1,
                        it.events[0].additionalCaptions.competitor2,
                        it.events[0].liveData.elapsed,
                        CustomViewTypes.GameView
                    )
                )
            }
        }
    }

    fun clearData() {
        uiModel.games.clear()
        uiModel.headlines.clear()
    }

    //update functions
    fun initTimerForUiUpdate(token: String) {
        object : CountDownTimer(
            context.resources.getInteger(R.integer.Time_10s).toLong(),
            context.resources.getInteger(R.integer.Time_1s).toLong()
        ) {

            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                viewModelScope.launch {
                    updateUi(token)
                    result.postValue(ResultOf.Success(uiModel))
                    initTimerForUiUpdate(token)
                }
            }
        }.start()
    }

    private suspend fun updateUi(token: String) = withContext(Dispatchers.IO) {
        try {
            listOf(
                launch { uiModel.games.addAll(fetchNeededInfoFromUpdatedGames(token)) },
                launch { uiModel.headlines.addAll(fetchNeededInfoFromUpdateHeaders(token)) }
            ).joinAll()
            false
        } catch (e: Throwable) {
            true
        }
    }


    private suspend fun fetchNeededInfoFromUpdateHeaders(token: String): List<HeadlinesModel> {
        val response: MutableList<HeadlinesModel> = mutableListOf()
        repo.getUpdateHeadlines(token).let { data ->
            if (data.isSuccessful) {
                fillCustomModelHeaders(data, response)
            } else {
                result.postValue(ResultOf.Failure(context.getString(R.string.call_error)))
            }
        }
        return response
    }

    private suspend fun fetchNeededInfoFromUpdatedGames(token: String): MutableList<CustomViewTypes> {
        val gamesModel: MutableList<CustomViewTypes> = mutableListOf()
        repo.getUpdateGames(token).let {
            if (it.isSuccessful) {
                fillCustomModelGames(gamesModel, it)
            } else {
                result.postValue(ResultOf.Failure(context.getString(R.string.call_error)))
            }
            return gamesModel
        }
    }
}